library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity iface_tx_sr9 is
    port (
        iClk: in std_logic;

        iNd: in std_logic;
        iData: in std_logic_vector (71 downto 0);

        oData: out std_logic_vector (7 downto 0)
    );
end entity;

architecture v1 of iface_tx_sr9 is

    type tAr is array (8 downto 0) of std_logic_vector (7 downto 0);
    signal sAr: tAr;
    signal sSr: tAr;

begin

    oData <= sSr (0);

    ass_loop: for i in 8 downto 0 generate
        sAr (i) <= iData ((i+1)*8-1 downto i*8);
    end generate;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iNd = '1' then
                sSr <= sAr;
            else
                sSr (8 downto 0) <= "00000000" & sSr (8 downto 1);
            end if;
        end if;
    end process;

end v1;
