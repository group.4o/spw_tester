library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity iface_tx_rd is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iSet: in std_logic;
        iAllow_read: in std_logic;

        oRd: out std_logic
    );
end entity;

architecture v1 of iface_tx_rd is

    signal sState: std_logic;

begin

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                oRd <= '0';
            else
                if sState = '1' and iAllow_read = '1' then
                    oRd <= '1';
                else
                    oRd <= '0';
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sState <= '0';
            else
                if sState = '0' then
                    if iSet = '1' then
                        sState <= '1';
                    end if;
                else
                    if iAllow_read = '1' then
                        sState <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end v1;
