library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity iface_rx is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iNd: in std_logic;
        iData: in std_logic_vector (7 downto 0);

        -- cmds
        oReset: out std_logic;
        oStart: out std_logic
    );
end entity;

architecture v1 of iface_rx is

    component iface_rx_cmp
        generic (
            gVal: std_logic_vector (7 downto 0)
        );
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iNd: in std_logic;
            iData: in std_logic_vector (7 downto 0);

            oNd: out std_logic
        );
    end component;

begin

    cmp_reset: iface_rx_cmp
        generic map (
            gVal => x"aa"
        )
        port map (
            iClk => iClk,
            iReset => iReset,

            iNd => iNd,
            iData => iData,

            oNd => oReset
        );

    cmp_start: iface_rx_cmp
        generic map (
            gVal => x"00"
        )
        port map (
            iClk => iClk,
            iReset => iReset,

            iNd => iNd,
            iData => iData,

            oNd => oStart
        );

end v1;
