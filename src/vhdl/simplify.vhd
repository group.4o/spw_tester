library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity simplify is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iVector_nd: in std_logic_vector;
        iVector_data: in std_logic_vector (63 downto 0);

        oFifo_wr: out std_logic;
        oFifo_data: out std_logic_vector (71 downto 0)
    );
end entity;

architecture v1 of simplify is

    constant cNum: integer := iVector_nd'length;

    type tCode_vector is array (cNum-1 downto 0)
        of std_logic_vector (7 downto 0);
    type tCode_vector_t is array (7 downto 0)
        of std_logic_vector (cNum-1 downto 0);

    function fill return tCode_vector is
        variable vRet: tCode_vector;
    begin
        for i in cNum-1 downto 0 loop
            vRet (i) := std_logic_vector (to_unsigned(
                i, 8
            ));
        end loop;
        return vRet;
    end function;

    constant cCode_vector: tCode_vector := fill;
    signal sCode_vector: tCode_vector;
    signal sCode_vector_t: tCode_vector_t;

    signal sNd: std_logic;
    signal sData: std_logic_vector (63 downto 0);
    signal sCode: std_logic_vector (7 downto 0);

begin

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sNd <= '0';
            else
                if iVector_nd = (iVector_nd'range => '0') then
                    sNd <= '0';
                else
                    sNd <= '1';
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sData <= (others => '0');
            else
                sData <= iVector_data;
            end if;
        end if;
    end process;

    code_assi: for i in cNum-1 downto 0 generate

        process (iClk)
        begin
            if iClk'event and iClk = '1' then
                if iVector_nd (i) = '0' then
                    sCode_vector (i) <= (others => '0');
                else
                    sCode_vector (i) <= cCode_vector (i);
                end if;
            end if;
        end process;

        code_assj: for j in 7 downto 0 generate
            sCode_vector_t (j)(i) <= sCode_vector (i)(j);
        end generate;

    end generate;

    code_ass: for i in 7 downto 0 generate

        process (iClk)
        begin
            if iClk'event and iClk = '1' then
                if iReset = '1' then
                    sCode (i) <= '0';
                else
                    if sCode_vector_t (i) = (cNum-1 downto 0 => '0') then
                        sCode (i) <= '0';
                    else
                        sCode (i) <= '1';
                    end if;
                end if;
            end if;
        end process;

    end generate;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                oFifo_wr <= '0';
            else
                oFifo_wr <= sNd;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                oFifo_data (71 downto 8) <= (others => '0');
            else
                oFifo_data (71 downto 8) <= sData;
            end if;
        end if;
    end process;

    oFifo_data (7 downto 0) <= sCode;

end v1;
