library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity iface is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        -- iface
        iNd: in std_logic;
        iData: in std_logic_vector (7 downto 0);
        iAllow_read: in std_logic;
        oNd: out std_logic;
        oData: out std_logic_vector (7 downto 0);

        -- ext fifo
        oFifo_rd: out std_logic;
        iFifo_nd: in std_logic;
        iFifo_data: in std_logic_vector (71 downto 0);

        oFifo_wr: out std_logic;
        oFifo_data: out std_logic_vector (71 downto 0);
        -- data for fifo
        iVector_nd: in std_logic_vector;
        iVector_data: in std_logic_vector;

        -- cmds
        oReset: out std_logic;
        oStart: out std_logic
    );
end iface;

architecture v1 of iface is

    component iface_tx
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iAllow_read: in std_logic;

            -- ext fifo
            oFifo_rd: out std_logic;
            iFifo_nd: in std_logic;
            iFifo_data: in std_logic_vector (71 downto 0);

            oNd: out std_logic;
            oData: out std_logic_vector (7 downto 0)
        );
    end component;

    component iface_rx
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iNd: in std_logic;
            iData: in std_logic_vector (7 downto 0);

            -- cmds
            oReset: out std_logic;
            oStart: out std_logic
        );
    end component;

    component simplify is
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iVector_nd: in std_logic_vector;
            iVector_data: in std_logic_vector (63 downto 0);

            oFifo_wr: out std_logic;
            oFifo_data: out std_logic_vector (71 downto 0)
        );
    end component;

begin

    tx: iface_tx
        port map (
            iClk => iClk,
            iReset => iReset,

            iAllow_read => iAllow_read,

            -- ext fifo
            oFifo_rd => oFifo_rd,
            iFifo_nd => iFifo_nd,
            iFifo_data => iFifo_data,

            oNd => oNd,
            oData => oData
        );

    rx: iface_rx
        port map (
            iClk => iClk,
            iReset => iReset,

            iNd => iNd,
            iData => iData,

            -- cmds
            oReset => oReset,
            oStart => oStart
        );

    s: simplify
        port map (
            iClk => iClk,
            iReset => iReset,

            iVector_nd => iVector_nd,
            iVector_data => iVector_data,

            oFifo_wr => oFifo_wr,
            oFifo_data => oFifo_data
        );

end v1;
