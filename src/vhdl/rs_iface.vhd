library ieee;
use ieee.std_logic_1164.all;


entity rs_iface is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        -- iface
        iRs: in std_logic;
        oRs: out std_logic;

        iEvent: in std_logic_vector (10 downto 0);

        -- ext fifo
        oFifo_rd: out std_logic;
        iFifo_nd: in std_logic;
        iFifo_data: in std_logic_vector (71 downto 0);

        oFifo_wr: out std_logic;
        oFifo_data: out std_logic_vector (71 downto 0);

        -- cmds
        oReset: out std_logic;
        oStart: out std_logic
    );
end entity;

architecture v1 of rs_iface is

    component rs_wrap
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iRs: in std_logic;
            oRs: out std_logic;

            iNd: in std_logic;
            iData: in std_logic_vector (7 downto 0);
            oHalf_empty: out std_logic;

            oNd: out std_logic;
            oData: out std_logic_vector (7 downto 0)
        );
    end component;

    component iface is
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            -- iface
            iNd: in std_logic;
            iData: in std_logic_vector (7 downto 0);
            iAllow_read: in std_logic;
            oNd: out std_logic;
            oData: out std_logic_vector (7 downto 0);

            -- ext fifo
            oFifo_rd: out std_logic;
            iFifo_nd: in std_logic;
            iFifo_data: in std_logic_vector (71 downto 0);

            oFifo_wr: out std_logic;
            oFifo_data: out std_logic_vector (71 downto 0);
            -- data for fifo
            iVector_nd: in std_logic_vector;
            iVector_data: in std_logic_vector;

            -- cmds
            oReset: out std_logic;
            oStart: out std_logic
        );
    end component;

    component timer
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iStart: in std_logic;

            oTime: out std_logic_vector
        );
    end component;

    signal sRx_nd: std_logic;
    signal sRx_data: std_logic_vector (7 downto 0);

    signal sHalf_empty: std_logic;
    signal sTx_nd: std_logic;
    signal sTx_data: std_logic_vector (7 downto 0);

    signal sTime: std_logic_vector (63 downto 0);

    signal sStart: std_logic;

begin

    oStart <= sStart;

    rs: rs_wrap
        port map (
            iClk => iClk,
            iReset => iReset,

            iRs => iRs,
            oRs => oRs,

            iNd => sTx_nd,
            iData => sTx_data,
            oHalf_empty => sHalf_empty,

            oNd => sRx_nd,
            oData => sRx_data
        );

    proto: iface
        port map (
            iClk => iClk,
            iReset => iReset,

            -- iface
            iNd => sRx_nd,
            iData => sRx_data,
            iAllow_read => sHalf_empty,
            oNd => sTx_nd,
            oData => sTx_data,

            -- ext fifo
            oFifo_rd => oFifo_rd,
            iFifo_nd => iFifo_nd,
            iFifo_data => iFifo_data,

            oFifo_wr => oFifo_wr,
            oFifo_data => oFifo_data,
            -- data for fifo
            iVector_nd => iEvent,
            iVector_data => sTime,

            -- cmds
            oReset => oReset,
            oStart => sStart
        );

    t: timer
        port map (
            iClk => iClk,
            iReset => iReset,

            iStart => sStart,

            oTime => sTime
        );

end v1;
