library ieee;
use ieee.std_logic_1164.all;


entity iface_tx is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iAllow_read: in std_logic;

        -- ext fifo
        oFifo_rd: out std_logic;
        iFifo_nd: in std_logic;
        iFifo_data: in std_logic_vector (71 downto 0);

        oNd: out std_logic;
        oData: out std_logic_vector (7 downto 0)
    );
end entity;

architecture v1 of iface_tx is

    component iface_tx_sr9
        port (
            iClk: in std_logic;

            iNd: in std_logic;
            iData: in std_logic_vector (71 downto 0);

            oData: out std_logic_vector (7 downto 0)
        );
    end component;

    component iface_tx_nd
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iNd: in std_logic;

            oNd: out std_logic;
            oDone: out std_logic
        );
    end component;

    component iface_tx_rd
        port (
            iClk: in std_logic;
            iReset: in std_logic;

            iSet: in std_logic;
            iAllow_read: in std_logic;

            oRd: out std_logic
        );
    end component;

    signal sDone: std_logic;

begin

    sr: iface_tx_sr9
        port map (
            iClk => iClk,

            iNd => iFifo_nd,
            iData => iFifo_data,

            oData => oData
        );

    cnt: iface_tx_nd
        port map (
            iClk => iClk,
            iReset => iReset,

            iNd => iFifo_nd,

            oNd => oNd,
            oDone => sDone
        );

    rd: iface_tx_rd
        port map (
            iClk => iClk,
            iReset => iReset,

            iSet => sDone,
            iAllow_read => iAllow_read,

            oRd => oFifo_rd
        );

end v1;
