library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity iface_rx_cmp is
    generic (
        gVal: std_logic_vector (7 downto 0)
    );
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iNd: in std_logic;
        iData: in std_logic_vector (7 downto 0);

        oNd: out std_logic
    );
end entity;

architecture v1 of iface_rx_cmp is
begin

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                oNd <= '0';
            else
                if iNd = '1' and iData = gVal then
                    oNd <= '1';
                else
                    oNd <= '0';
                end if;
            end if;
        end if;
    end process;

end v1;
