library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity timer is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iStart: in std_logic;

        oTime: out std_logic_vector
    );
end entity;

architecture v1 of timer is

    signal sState: std_logic;
    signal sAdd: unsigned (0 downto 0);
    signal sCnt: unsigned (oTime'range);

begin

    oTime <= std_logic_vector (sCnt);

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sState <= '0';
            else
                if iStart = '1' then
                    sState <= '1';
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if sState = '0' then
                sCnt <= (others => '0');
            else
                sCnt <= sCnt +1;
            end if;
        end if;
    end process;

end v1;
