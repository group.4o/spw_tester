library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


entity iface_tx_nd is
    port (
        iClk: in std_logic;
        iReset: in std_logic;

        iNd: in std_logic;

        oNd: out std_logic;
        oDone: out std_logic
    );
end entity;

architecture v1 of iface_tx_nd is

    signal sCnt: unsigned (2 downto 0);
    signal sCnt_done: std_logic;

    signal sState: std_logic;

begin

    oNd <= sState;
    oDone <= sCnt_done;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sState <= '0';
            else
                if sState = '0' then
                    if iNd = '1' then
                        sState <= '1';
                    end if;
                else
                    if sCnt_done = '1' then
                        sState <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if sState = '0' then
                sCnt <= (others => '0');
            else
                sCnt <= sCnt + 1;
            end if;
        end if;
    end process;

    process (iClk)
    begin
        if iClk'event and iClk = '1' then
            if iReset = '1' then
                sCnt_done <= '0';
            else
                if sCnt = "110" then
                    sCnt_done <= '1';
                else
                    sCnt_done <= '0';
                end if;
            end if;
        end if;
    end process;

end v1;
